#!/bin/bash

sudo /home/pi/Desktop/komeye/Pi-Eyes/fbx2 -o -b 4000000 > /home/pi/Desktop/Logs/fbx2.log 2>&1 &

#cd /home/pi/Desktop/komeye/Pi-LEDs
#sudo python3 leds.py > /home/pi/Desktop/Logs/leds.log 2>&1

myscript()
{
	cd /home/pi/Desktop/komeye/Pi-Eyes
   	sudo python -u eyes.py --radius 128 > /home/pi/Desktop/Logs/eyes.log 2>&1
}

until myscript; do
    	echo "'myscript' crashed with exit code $?. Restarting..." >&2
   	sleep 1
done

#cd /home/pi/Desktop/komeye/Pi-Eyes
#sudo python -u eyes.py --radius 128 > /home/pi/Desktop/Logs/eyes.log 2>&1
