#!/usr/bin/python

import sys

new_path = 'home/pi/Desktop/komeye/Pi-Eyes'
if new_path not in sys.path:
    sys.path.append(new_path)

from eyes_include import *

# Socket server -----------------------------------------------------------
host = '192.168.1.111'
port = 5432
olddata= 'B'

number_of_attemps = 0
#while number_of_attemps < 5:
server_socket = None
while True:
    try:
        print "Creating socket server..."
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_socket.settimeout(0.01)
        server_socket.bind((host, port))
        time.sleep(1)

    except:
        print "Error creating socket server"
        server_socket = None
        time.sleep(1)

        number_of_attemps = number_of_attemps + 1
        
        frame(PUPIL_MAX, 10)
        set_led_color_on_icon(10)

        #continue
        #break

    else:
        break
        
print "Socket server is up"

# Data handler thread ----------------------------------------------------
data_received = None

def get_data():

    global server_socket
    global data_received

    try:
        data_received = server_socket.recv(1024)

    except socket.timeout, e:
        #print e
        data_received = -2

    except socket.error,e:
        #print e
        data_received = -3

# MAIN LOOP -- runs continuously -----------------------------------------
try:
    refresh_rate = EYES_REFRESH_RATE #Eyes refresh rate (seconds)

    last_refresh_time = time.time()

    got_new_icon = False
    icon = -1
    last_icon = -1
    last_icon_time = time.time()
    new_icon_time = time.time()
    
    print "Receiving data..."

    while True:
    
        get_data()
        #print data_received
        
        if data_received == b'':
			print "Socket connection broken"
			break
            
        if data_received != None:
        
            if data_received == -2:
                icon = last_icon

            elif data_received == -3:
                icon = 11
                refresh_rate = ICONS_REFRESH_RATE

            else:
                dat = data_received.decode()
                k = int(dat)
                icon = k-97
                #print(time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime()) + " ---------- " + str(icon))
                
                if icon < -1 or icon > len(icons_list)-1:
                    icon = -1
                    refresh_rate = EYES_REFRESH_RATE
                    #print "Received unusuable data"

                elif icon == -1:
                    refresh_rate = EYES_REFRESH_RATE

                else:
                    refresh_rate = ICONS_REFRESH_RATE
 
                got_new_icon = True
                new_icon_time = time.time()
                #print "new icon"

            last_icon_time = time.time()
            last_icon = icon

        #Get current time and update frame
        dt_refresh = time.time() - last_refresh_time
        
        #if got_new_icon:
        if dt_refresh >= refresh_rate or got_new_icon:
            last_refresh_time = time.time()
            got_new_icon = False
            
            if icon is not len(icons_list) - 1:
                DISPLAYING_BLACK_SCREEN = False

            #Display black icon after a while
            if time.time() - new_icon_time > SCREEN_OFF_TIMEOUT:
                icon = len(icons_list) - 1
                last_icon = icon
                refresh_rate = SCREEN_OFF_REFRESH_RATE
                
                if not DISPLAYING_BLACK_SCREEN:
                    #print(time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime()) + " ---------- black screen")
                    DISPLAYING_BLACK_SCREEN = True

            #Display dimmed icon after a short while
            elif time.time() - new_icon_time > DIM_ICON_TIMEOUT:
                if icon is not -1:
                    icon = icon + len(icons_list)

        #print("Display icon: " + str(icon))
        
        frame(PUPIL_MAX, icon)
        set_led_color_on_icon(icon)
        
    print "Exited main while loop"

except KeyboardInterrupt:
    pass

except:
    print "Error in main loop"
    pass
    
print "Eyes program stopped running"

DISPLAY.stop()
GPIO.cleanup()
