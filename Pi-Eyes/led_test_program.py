#!/usr/bin/python

import time
import RPi.GPIO as GPIO

# LED control variables
green_leds = [17, 25]
red_leds = [18, 27]

# GPIO initialization ------------------------------------------------------
GPIO.setmode(GPIO.BCM)

for led in green_leds:
	GPIO.setup(led, GPIO.OUT)
   
for led in red_leds:
	GPIO.setup(led, GPIO.OUT)

def set_led_color(color):
	if color=="green":
		for led in green_leds:
			GPIO.output(led, GPIO.LOW)

		for led in red_leds:
			GPIO.output(led, GPIO.HIGH)

	if color=="red":
		for led in green_leds:
			GPIO.output(led, GPIO.HIGH)

		for led in red_leds:
			GPIO.output(led, GPIO.LOW)

	if color=="orange":
		for led in green_leds:
			GPIO.output(led, GPIO.LOW)

		for led in red_leds:
			GPIO.output(led, GPIO.LOW)

	if color=="none":
		for led in green_leds:
			GPIO.output(led, GPIO.HIGH)

		for led in red_leds:
			GPIO.output(led, GPIO.HIGH)

while True:
	set_led_color("orange")
	time.sleep(0.5)

	set_led_color("red")
	time.sleep(0.5)

	set_led_color("green")
	time.sleep(0.5)
